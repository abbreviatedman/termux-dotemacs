;; stop cluttering up my config file!
(setq custom-file "~/.custom.el")

;; set up package system
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-expand-minimally t)

(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; General Appearance
(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)
(menu-bar-mode -1)

;; Clipboard
(global-set-key (kbd "C-x y") #'yank-from-kill-ring)

(use-package emacs
  :init
  (setq modus-themes-bold-constructs t
        modus-themes-italic-constructs t
        modus-themes-syntax '(alt-syntax yellow-comments green-strings)
        modus-themes-paren-match '(intense underline)
        modus-themes-subtle-line-numbers nil
        modus-themes-deuteranopia t
        modus-themes-markup '(background)
        modus-themes-org-blocks 'gray-background
        modus-themes-region '(no-extend bg-only accented)
        modus-themes-hl-line nil
        modus-themes-headings (quote
                               ((1 . (rainbow 1.8))
                                (2 . (rainbow 1.6))
                                (3 . (rainbow 1.4))
                                (4 . (rainbow 1.2))))
        modus-themes-completions (quote
                                  ((matches . (intense background underline bold))
                                   (selection . (accented intense bold))
                                   (popup . (accented intense bold)))))

  :config
  (mapc #'disable-theme custom-enabled-themes)
  (load-theme 'modus-operandi t)
  (set-face-attribute 'modus-themes-hl-line nil
                      :extend nil
                      :background 'unspecified))

;; Terminals don't let apps change the cursor. So this package asks the terminal if it might not consider doing it
(use-package evil-terminal-cursor-changer
  :config
  (evil-terminal-cursor-changer-activate)
  (setq evil-motion-state-cursor 'box
        evil-visual-state-cursor 'box
        evil-normal-state-cursor 'box
        evil-insert-state-cursor 'bar
        evil-emacs-state-cursor  'hbar))

(use-package evil
  :config
  (evil-mode 1))

(use-package which-key
  :config
  (which-key-mode))

(use-package vertico
  :init
  (vertico-mode)
  :config
  (setq vertico-cycle t)
  (vertico-indexed-mode)
  (global-set-key (kbd  "C-x \"") #'vertico-repeat-select)
  (define-key vertico-map (kbd "C-s-n") #'vertico-scroll-down)
  (define-key vertico-map (kbd "C-s-j") #'vertico-scroll-down)
  (define-key vertico-map (kbd "C-s-p") #'vertico-scroll-up)
  (define-key vertico-map (kbd "C-s-k") #'vertico-scroll-up))

(use-package corfu
  :hook ((prog-mode . corfu-mode)
         (eshell-mode . corfu-mode)
         (vterm-mode . corfu-mode))
  :config
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify)
  (setq corfu-commit-predicate nil
        completion-category-defaults nil
        completion-category-overrides nil
        corfu-auto t
        corfu-auto-prefix 1
        corfu-quit-no-match t
        corfu-cycle t)

  (evil-global-set-key 'insert (kbd "C-SPC") #'complete-symbol)
  (define-key corfu-map (kbd "M-g") #'corfu-quit)
  (define-key corfu-map (kbd "C-n") #'corfu-next)
  (define-key corfu-map (kbd "C-j") #'corfu-next)
  (define-key corfu-map (kbd "C-p") #'corfu-previous)
  (define-key corfu-map (kbd "C-k") #'corfu-previous)
  (define-key corfu-map (kbd "C-S-n") #'corfu-scroll-up)
  (define-key corfu-map (kbd "C-S-j") #'corfu-scroll-up)
  (define-key corfu-map (kbd "C-S-p") #'corfu-scroll-down)
  (define-key corfu-map (kbd "C-S-k") #'corfu-scroll-down)
  (define-key corfu-map (kbd "RET") nil)
  (define-key corfu-map (kbd "RET") #'corfu-complete)
  (define-key corfu-map (kbd "TAB") nil)
  (define-key corfu-map (kbd "<tab>") nil)
  (define-key corfu-map (kbd "<tab>") #'corfu-insert)
  (define-key corfu-map (kbd "TAB") #'corfu-insert)

  (defun corfu-beginning-of-prompt ()
    "Move to beginning of completion input."
    (interactive)
    (corfu--goto -1)
    (goto-char (car completion-in-region--data)))

  (defun corfu-end-of-prompt ()
    "Move to end of completion input."
    (interactive)
    (corfu--goto -1)
    (goto-char (cadr completion-in-region--data)))

  (define-key corfu-map (kbd "^") #'corfu-beginning-of-prompt)
  (define-key corfu-map (kbd "$") #'corfu-end-of-prompt))

;;; TODO configure consult.
;;; TODO add key binding for consult-recent-files

;; (use-package consult
;;   :config
;;   (global-set-key (kbd "C-_") #'consult-line)
;;   (plist-put (alist-get 'perl consult-async-split-styles-alist) :initial "_"))

(use-package orderless
  :init
  (defun crj/vertico-orderless-dispatch (pattern _index _total)
    "Taken from Doom Emacs."
    (cond
     ;; Ensure $ works with Consult commands, which add disambiguation suffixes
     ((string-suffix-p "$" pattern)
      `(orderless-regexp . ,(concat (substring pattern 0 -1) "[\x200000-\x300000]*$")))
     ;; Ignore single !
     ((string= "!" pattern) `(orderless-literal . ""))
     ;; Without literal
     ((string-prefix-p "!" pattern) `(orderless-without-literal . ,(substring pattern 1)))
     ;; Character folding
     ((string-prefix-p "%" pattern) `(char-fold-to-regexp . ,(substring pattern 1)))
     ((string-suffix-p "%" pattern) `(char-fold-to-regexp . ,(substring pattern 0 -1)))
     ;; Initialism matching
     ((string-prefix-p "`" pattern) `(orderless-initialism . ,(substring pattern 1)))
     ((string-suffix-p "`" pattern) `(orderless-initialism . ,(substring pattern 0 -1)))
     ;; Literal matching
     ((string-prefix-p "=" pattern) `(orderless-literal . ,(substring pattern 1)))
     ((string-suffix-p "=" pattern) `(orderless-literal . ,(substring pattern 0 -1)))
     ;; Flex matching
     ((string-prefix-p "~" pattern) `(orderless-flex . ,(substring pattern 1)))
     ((string-suffix-p "~" pattern) `(orderless-flex . ,(substring pattern 0 -1)))))

  (setq completion-ignore-case t
        completion-styles '(orderless basic)
        completion-category-defaults nil
        orderless-component-separator "\_"
        orderless-style-dispatchers '(crj/vertico-orderless-dispatch)
        orderless-matching-styles '(orderless-flex orderless-literal orderless-regexp)
        completion-category-overrides '((file (styles partial-completion)))))

(setq read-extended-command-predicate #'command-completion-default-include-p)

(use-package cape
  ;; Bind dedicated completion commands
  :bind (("C-c p" . completion-at-point))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-tex)
  ;; re-enable if you want to use completion for dabbrev
  ;; (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-sgml)
  (add-to-list 'completion-at-point-functions #'cape-rfc1345)
  (add-to-list 'completion-at-point-functions #'cape-symbol))

(defun crj/git-cloud-save ()
  "Adds, commits, and pushes without any further input from the user.

Adapted from this SO answer:
https://emacs.stackexchange.com/questions/21597/using-magit-for-the-most-basic-add-commit-push/64991#64991.

Only changes were: 1. Removing the command to save all open
buffers. We /could/ save the visited buffer only, though even that
should likely be a discrete operation.  2. Removing user input from
the commit message altogether.  3.  Disabling the pop-up git status
window. (It still shows in the minibuffer, as well as the buffer
`shell-command-buffer-name'.)"
  (interactive)
  (magit-stage-modified)
  (let
      ((display-buffer-alist
        '(shell-command-buffer-name '(#'display-buffer-no-window))))
    (shell-command
     (format "git commit -m \"Updates org-stuff.\""))
    (shell-command "git push")))

(use-package magit
  :config
  (setq magit-section-disable-line-numbers nil)
  (define-key (current-global-map) (kbd "C-c p") #'crj/git-cloud-save)
  (define-key (current-global-map) (kbd "C-c f") #'magit-pull))

(use-package org
  :config
  (setq
   org-todo-keywords '((sequence "TODO" "NEXT" "|" "DONE"))))

(use-package recentf
  :init
  :config
  (setq recentf-max-saved-items 1000
        recentf-auto-cleanup 'never)

  (recentf-mode)
  (global-set-key (kbd "C-x C-r") #'consult-recent-file))

(defun crj/agenda () (interactive) (org-agenda t "g"))
;; this is for a work-only version I can display at work... note I haven't made the actual custom agenda command yet!
(defun crj/work-agenda () (interactive) (org-agenda t "w"))
(setq org-agenda-start-day nil
      org-agenda-files '("~/org-stuff/inbox.org"
                         "~/org-stuff/readme.org"
                         "~/org-stuff/personal.org"
                         "~/org-stuff/archive.org")
      org-agenda-custom-commands '(("g" "Daily agenda and top priority tasks"
                                    ((todo "WAIT"
                                           ((org-agenda-overriding-header "Tasks On Hold\n")
                                            (org-agenda-block-separator nil)))
                                     (todo "TODO"
                                           ((org-agenda-files '("~/org-stuff/inbox.org"))
                                            (org-agenda-block-separator nil)
                                            (org-agenda-overriding-header "\nInbox Items\n")))
                                     (tags-todo "*"
                                                ((org-agenda-skip-function '(org-agenda-skip-if nil '(timestamp)))
                                                 (org-agenda-skip-function
                                                  `(org-agenda-skip-entry-if
                                                    'notregexp ,(format "\\[#%s\\]" (char-to-string org-priority-highest))))
                                                 (org-agenda-block-separator nil)
                                                 (org-agenda-overriding-header "\nImportant Without Deadline\n")))
                                     (agenda "" ((org-agenda-span 1)
                                                 (org-deadline-warning-days 0)
                                                 (org-agenda-block-separator nil)
                                                 (org-scheduled-past-days 0)
                                                 (org-agenda-day-face-function (lambda (_) 'org-agenda-date))
                                                 (org-agenda-format-date "%A %-e %B %Y")
                                                 (org-agenda-overriding-header "\nToday's Agenda\n")))
                                     (agenda "" ((org-agenda-start-on-weekday nil)
                                                 (org-agenda-start-day "+1d")
                                                 (org-agenda-span 3)
                                                 (org-deadline-warning-days 0)
                                                 (org-agenda-block-separator nil)
                                                 (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                                                 (org-agenda-overriding-header "\nNext Three Days After\n")))
                                     (agenda "" ((org-agenda-time-grid nil)
                                                 (org-agenda-start-on-weekday nil)
                                                 (org-agenda-start-day "+4d")
                                                 (org-agenda-span 14)
                                                 (org-agenda-show-all-dates nil)
                                                 (org-deadline-warning-days 0)
                                                 (org-agenda-block-separator nil)
                                                 (org-agenda-entry-types '(:deadline))
                                                 (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                                                 (org-agenda-overriding-header "\nUpcoming Deadlines (+14d After)\n")))))))

(global-set-key (kbd "C-c g") #'crj/agenda)
(global-set-key (kbd "C-c a") #'org-agenda)
